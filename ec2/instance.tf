#Define the provider
provider "aws" {
	# access_key = var.access_key
	# secret_key = var.secret_key
  region = var.region
}

#Create a virtual network
resource "aws_vpc" "vpc_demo" {
  count = var.environment == "Production" ? 1 : 0
  cidr_block           = var.cidr_vpc
  enable_dns_hostnames = "true"
  tags = merge(local.common_tags, { Name = "${var.environment}-vpc" })
}

resource "aws_internet_gateway" "igw" {
  vpc_id = var.vpc_id
  tags   = merge(local.common_tags, { Name = "${var.environment}-igw" })
}

resource "aws_subnet" "subnet-demo" {
  tags   = merge(local.common_tags, { Name = "${var.environment}-subnet" })
  vpc_id = var.vpc_id
  cidr_block              = var.cidr_subnet
  map_public_ip_on_launch = true
  depends_on              = [aws_vpc.vpc_demo]
  availability_zone = var.availability_zone
}

resource "aws_route_table" "rtb" {
  vpc_id = var.vpc_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
  tags = merge(local.common_tags, { Name = "${var.environment}-rtb" })
}

resource "aws_route_table_association" "rta-subnet" {
  subnet_id = var.subnet_id
  route_table_id = aws_route_table.rtb.id
}


# SECURITY GROUP
resource "aws_security_group" "nginx-sg" {
  name   = "nginx_sg"
  vpc_id = var.vpc_id

  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTP access from anywhere
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = merge(local.common_tags, { Name = "${var.environment}-nginx" })
}

#Create a private key which can be used to login to the webserver
resource "tls_private_key" "keypair" {
  algorithm = "RSA"
}

#Save public key attributes from the generated key
resource "aws_key_pair" "keypair" {
  key_name   = var.keypair
  public_key = tls_private_key.keypair.public_key_openssh
}

#Save the key to your local system
resource "local_file" "localkey" {
  content  = tls_private_key.keypair.private_key_pem
  filename = "${var.keypair}.pem"
}

#Create your webserver instance
resource "aws_instance" "web" {
  ami           = var.instance_ami
  instance_type = var.instance_type

  subnet_id = var.subnet_id
  key_name        = var.keypair
  security_groups = [aws_security_group.nginx-sg.id]

  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      user        = "ec2-user"
      private_key = tls_private_key.keypair.private_key_pem
      host        = aws_instance.web.public_ip
    }
    inline = [
      "sudo yum install docker -y",
      "sudo service docker start",
      "sudo mkdir ~/site-content",
      "sudo docker run --name web -d -p 80:80 -v ~/site-content:/usr/share/nginx/html:ro nginx:latest",
      "cip='sudo docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' web'",
      "IP_ADDR = $(sudo docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' web)",      
      "sudo docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' web | sudo tee ~/site-content/ip.txt",      
      "echo '<html><head><title> Demo </title></head><body style=\"background-color:#1F778D\"><p style=\"text-align: center;\"><span style=\"color:#FFFFFF;\"><span style=\"font-size:28px;\">Hello from the Nginx Container inside EC2 Instance </span></span></p></body></html>' | sudo tee ~/site-content/index.html"
    ]
  }
  tags = merge(local.common_tags, { Name = "${var.environment}-WebServer" })
}

output "aws_instance_public_dns" {
  value = aws_instance.web.public_dns
  description = "Public DNS of the Nginx Server Instance"
}
output "aws_instance_public_ip" {
  value = aws_instance.web.public_ip
  description = "Public IP of the Nginx Server Instance"
}

output "aws_instance_private_ip" {
  value = aws_instance.web.private_ip
  description = "Private IP of the Nginx Server Instance"
}

resource "null_resource" "get_ip" {
  provisioner "local-exec" {
    command = "curl -o ip.txt ${aws_instance.web.public_ip}/ip.txt"
  }
}

data "local_file" "ip"{
  filename = "ip.txt"
  depends_on = [ null_resource.get_ip ]
}

output "container_ip" {
  value = data.local_file.ip.content
  depends_on = [ null_resource.get_ip ]
}

output "vpc_id" {
  value = aws_vpc.vpc_demo.id
}

output "subnet_id" {
  value = aws_subnet.subnet-demo.id
}