#Variables
# If you have configured AWS already, let it remain commented

# variable "access_key" {
# 	default = "ACCESSKEY"
# }
# variable "secret_key" {
# 	default = "SECRETKEY"
# }
variable "region" {
  default = "us-east-1"
}

variable "keypair" {}

variable "vpc_id" {}
variable "subnet_id" {}
variable "ami_id" {}

variable "cidr_vpc" {
  description = "CIDR block for the VPC"
  default = "10.0.0.0/16"
}
variable "cidr_subnet" {
  description = "CIDR block for the subnet"
  default = "10.0.1.0/24"
}
variable "availability_zone" {
  description = "availability zone to create subnet"
  default = "us-east-1a"
}

variable "instance_ami" {
  description = "AMI for aws EC2 instance"
  default = "ami-04d29b6f966df1537"
}
variable "instance_type" {
  description = "type for aws EC2 instance"
  default = "t2.micro"
}

variable "CreatedBy" {
  default = "Anil Pokhrel"
}
variable "CreatedTime" {
  default = ""
}
variable "environment" {
  default = "Development"
}

locals {
  common_tags = {
    CreatedBy   = var.CreatedBy
    CreatedTime = formatdate("MM-DD-YYYY hh:mm:ss", timestamp())
  }
}