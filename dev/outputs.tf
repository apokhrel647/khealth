output "public_dns" {
  value = module.my_ec2.aws_instance_public_dns
  description = "Public DNS of the Nginx Server Instance"
}

output "public_ip" {
  value = module.my_ec2.aws_instance_public_ip
  description = "Public IP of the Nginx Server Instance"
}

output "container_ip" {
  value = module.my_ec2.container_ip
  description = "Internal IP of the NGINX Container"
}