******************************************
NOTES
******************************************
The main module is in ec2 folder.
******************************************
Two environments were assumed: Development and Production
******************************************
The script automatically creates one Web Server Instance with the provided configuration.
******************************************
To change configuration, change main.tf inside dev folder for Development and inside prod folder for Production.
******************************************
Since it assumes 2 different environments, it creates EC2 instance on independent VPC.
******************************************
It first creates VPC, Internet Gateway, Subnet, Route Table, Route Table Association, Security Group only allowing Http and SSH.
******************************************
It also creates Key pair to log into the EC2 instance to further install Docker.
******************************************
Then it downloads the Docker image of Nginx:latest and runs the container exposing the port 80 and attaching volume ~/site-content:/usr/share/nginx/html:ro
******************************************
The index html file was created inside ~/site-content which is default for Nginx web server as we mounted /usr/share/nginx/html on it.
******************************************
Then, It outputs the PublicIP, Public DNS and Private IP of NGINX Container.
******************************************
******************************************
To launch the EC2 instance in Development 
Be inside khealth/dev folder

******************************************
To launch the EC2 instance in Production 
Be inside khealth/prod folder

******************************************
Then use following commands respectively.

terraform init
terraform plan -out plan.tfplan
terraform apply plan.tfplan --auto-approve

******************************************
To destroy, 
terraform destroy --auto-approve


******************************************
******************************************
COMMENTS
******************************************
The module doesn't support Count so, if you need to create multiple EC2 instances using the module, u need to define it multiple times.
******************************************
But if you use the count inside aws_instance, it will create multiple EC2 instances. 
******************************************
But the script needs to be tweaked if you want to specify definite subnets and availability zones for those EC2 instances.
******************************************
******************************************
If the purpose is to launch multiple Nginx instances as a part of common Web server, then the best approach is to launch them as a Elastic Auto Scaled instances with load balancing.
******************************************
# Somewhat like this
******************************************
# No of instances of EC2 instances
******************************************
resource "aws_instance" "nginx" { 

  count                  = var.instance_count

  ami                    = data.aws_ami.aws-linux.id

  instance_type          = "t2.micro"

  subnet_id              = aws_subnet.subnet[count.index % var.subnet_count].id

  vpc_security_group_ids = [aws_security_group.nginx-sg.id]

  key_name               = var.key_name

  iam_instance_profile   = aws_iam_instance_profile.nginx_profile.name

  depends_on             = [aws_iam_role_policy.allow_s3_all]

  connection {

    type        = "ssh"

    host        = self.public_ip

    user        = "ec2-user"

    private_key = file(var.private_key_path)

  }
  provisioner {

      ......................

      ......................

  }

}

******************************************
# Security group of Elastic load balancer
******************************************
resource "aws_security_group" "elb-sg" {

  name   = "nginx_elb_sg"

  vpc_id = aws_vpc.vpc.id

  #Allow HTTP from anywhere

  ingress {

    from_port   = 80

    to_port     = 80

    protocol    = "tcp"

    cidr_blocks = ["0.0.0.0/0"]

  }

  #allow all outbound

  egress {

    from_port   = 0

    to_port     = 0
    
    protocol    = "-1"

    cidr_blocks = ["0.0.0.0/0"]

  }

  tags = merge(local.common_tags, { Name = "${var.environment_tag}-elb-sg" })

}

******************************************
# load balancer
******************************************

resource "aws_elb" "web" {

  name = "nginx-elb"

  subnets         = aws_subnet.subnet[*].id

  security_groups = [aws_security_group.elb-sg.id]

  instances       = aws_instance.nginx[*].id

  listener {

    instance_port     = 80

    instance_protocol = "http"

    lb_port           = 80

    lb_protocol       = "http"

  }

  tags = merge(local.common_tags, { Name = "${var.environment_tag}-elb" })

}

******************************************
# displaying the dns name of elastic load balancer
******************************************

output "aws_elb_public_dns" {

value = aws_elb.web.dns_name

}