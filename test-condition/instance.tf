#Define the provider
provider "aws" {
	# access_key = var.access_key
	# secret_key = var.secret_key
  region = var.region
}

variable "environment" {
  default = "Development"
}

#Create a virtual network
resource "aws_vpc" "vpc_demo" {
  ## If environment equals Production is True, return 1 else 0
  count = var.environment == "Production" ? 1 : 0
  cidr_block           = var.cidr_vpc
  enable_dns_hostnames = "true"
  tags = merge(local.common_tags, { Name = "${var.environment}-vpc" })
}

output "vpc_id" {
  value = aws_vpc.vpc_demo
}
