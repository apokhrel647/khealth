#Variables
# If you have configured AWS already, let it remain commented

# variable "access_key" {
# 	default = "ACCESSKEY"
# }
# variable "secret_key" {
# 	default = "SECRETKEY"
# }
variable "region" {
  default = "us-east-1"
}

variable "cidr_vpc" {
  description = "CIDR block for the VPC"
  default = "10.0.0.0/16"
}

# variable "environment" {
#   default = "Production"
# }

variable "CreatedBy" {
  default = "Anil Pokhrel"
}

locals {
  common_tags = {
    CreatedBy   = var.CreatedBy
    CreatedTime = formatdate("MM-DD-YYYY hh:mm:ss", timestamp())
  }
}