module "my_ec2" {
  source        = "../ec2"
  cidr_vpc      = "10.0.0.0/16"
  vpc_id        = module.my_ec2.vpc_id
  cidr_subnet   = "10.0.2.0/24"
  ami_id        = "ami-0be2609ba883822ec"
  instance_type = "t2.micro"
  subnet_id     = module.my_ec2.subnet_id
  availability_zone = "us-east-1b"
  environment   = "Production"
  keypair = "key-prod"
}
